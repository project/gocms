api = 2
core = 7.x
includes[] = drupal-org-core.make

projects[gocms][type] = "profile"
projects[gocms][download][type] = "git"
projects[gocms][download][url] = "git@code.go1.com.au:profiles/gocms.git"
projects[gocms][download][branch] = "7.x-1.x"
