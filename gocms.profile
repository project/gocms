<?php
/**
 * @file
 */

require_once dirname(__FILE__) . '/gocms.inc';

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 */
function gocms_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];

  $form['features'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => st('Extra features'),
    '#description' => st('Please select extra features you would like to enable for your Drupal site.'),
  );

  $form['features']['preconfigured_features'] = array(
    '#tree' => TRUE,
    '#title' => st('Preconfigured Features'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'options' => array(
      '#type' => 'checkboxes',
      '#options' => gocms_available_preconfigured_features(),
    ),
  );

  $form['features']['modules'] = array(
    '#tree' => TRUE,
    '#title' => st('Modules'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'options' => array(
      '#type' => 'checkboxes',
      '#options' => gocms_available_modules(),
    ),
  );

  // Default values for site informations
  $form['site_information']['site_mail']['#default_value'] = 'info@go1.com.au';

  // Default values for admin account
  $form['admin_account']['account']['name']['#default_value'] = 'go_support';
  $form['admin_account']['account']['mail']['#default_value'] = 'info@go1.com.au';
  $form['admin_account']['account']['#attached']['js'] = array(
    drupal_get_path('profile', 'gocms') . '/misc/js/install_config.js' => array(),
  );

  $form['#submit'][] = 'gocms_form_install_configure_form_alter__submit';
}

/**
 * Custom submit handler for install-config form.
 */
function gocms_form_install_configure_form_alter__submit($form, $form_state) {
  $preconfigured_features = array_filter($form_state['values']['features']['preconfigured_features']['options']);
  if (!empty($preconfigured_features)) {
    foreach ($preconfigured_features as $preconfigured_feature) {
      gocms_enable_preconfigured_feature($preconfigured_feature);
    }
  }

  $modules = array_filter($form_state['values']['features']['modules']['options']);
  if (!empty($modules)) {
    module_enable($modules);
  }
}
