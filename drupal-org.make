api = 2
core = 7.x

; Must have
projects[context][subdir] = contrib
projects[context][version] = "7.x-3.1"
projects[ctools][subdir] = contrib
projects[ctools][version] = "7.x-1.3"
projects[entity][subdir] = contrib
projects[entity][version] = "7.x-1.2"
projects[features][subdir] = contrib
projects[features][version] = "7.x-2.0"
projects[libraries][subdir] = contrib
projects[libraries][version] = "7.x-2.1"
projects[views][subdir] = contrib
projects[views][version] = "7.x-3.7"

; Fields
projects[addressfield][subdir] = contrib
projects[addressfield][version] = "7.x-1.x-dev"
projects[date][subdir] = contrib
projects[date][version] = "7.x-2.6"
projects[email][subdir] = contrib
projects[email][version] = "7.x-1.2"
projects[field_collection][subdir] = contrib
projects[field_collection][version] = "7.x-1.x-dev"
projects[field_group][type] = contrib
projects[field_group][version] = "7.x-1.3"

; Fields » Media
projects[media][subdir] = contrib
projects[media][version] = "7.x-1.3"
projects[media_youtube][subdir] = contrib
projects[media_youtube][version] = "7.x-2.0-rc4"

; Admin
projects[module_filter][subdir] = contrib
projects[module_filter][version] = "7.x-2.0-alpha1"
projects[rules][subdir] = contrib
projects[rules][version] = "7.x-2.6"
projects[token][subdir] = contrib
projects[token][version] = "7.x-1.5"
projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = "7.x-3.1"

; Content Editors
projects[diff][subdir] = contrib
projects[diff][version] = "7.x-3.2"
projects[workbench][subdir] = contrib
projects[workbench][version] = "7.x-1.2"
projects[ckeditor][subdir] = contrib
projects[ckeditor][version] = "7.x-1.13"

; Developers
projects[devel][subdir] = contrib
projects[devel][version] = "7.x-1.x-dev"
projects[devel_themer][subdir] = contrib
projects[devel_themer][version] = "7.x-1.x-dev"
projects[search_krumo][subdir] = contrib
projects[search_krumo][version] = "7.x-1.x-dev"
projects[masquerade][subdir] = contrib
projects[masquerade][version] = "7.x-1.x-dev"

; Spam
projects[captcha][subdir] = contrib
projects[captcha][version] = "7.x-1.0"
projects[recaptcha][subdir] = contrib
projects[recaptcha][version] = "7.x-1.10"
projects[mollom][subdir] = contrib
projects[mollom][version] = "7.x-2.8"
projects[hidden_captcha][subdir] = contrib
projects[hidden_captcha][version] = "7.x-1.0"
projects[honeypot][subdir] = contrib
projects[honeypot][version] = "7.x-1.15"

; Search
projects[facetapi][subdir] = contrib
projects[facetapi][version] = "7.x-1.3"
projects[search_api][subdir] = contrib
projects[search_api][version] = "7.x-1.9"
projects[search_api_solr][subdir] = contrib
projects[search_api_solr][version] = "7.x-1.3"

; SEO
projects[fences][subdir] = contrib
projects[fences][version] = "7.x-1.0"
projects[pathauto][subdir] = contrib
projects[pathauto][version] = "7.x-1.12"
projects[google_analytics][subdir] = contrib
projects[google_analytics][version] = "7.x-1.4"
projects[xmlsitemap][subdir] = contrib
projects[xmlsitemap][version] = "7.x-2.0-rc2"
projects[metatag][subdir] = contrib
projects[metatag][version] = "7.x-1.0-beta7"

; Performance modules
projects[cache_actions][subdir] = contrib
projects[cache_actions][version] = "7.x-2.0-alpha5"
projects[advagg][subdir] = contrib
projects[advagg][version] = "7.x-2.3"
projects[cdn][subdir] = contrib
projects[cdn][version] = "7.x-2.6"
projects[entitycache][subdir] = contrib
projects[entitycache][version] = "7.x-1.2"
projects[speedy][subdir] = contrib
projects[speedy][version] = "7.x-1.8"
projects[views_content_cache][subdir] = contrib
projects[views_content_cache][version] = "7.x-3.0-alpha3"
projects[views_cache_bully][subdir] = contrib
projects[views_cache_bully][version] = "7.x-3.0"

; Themes
projects[omega][type] = "theme"
projects[omega][version] = "theme"

; Libraries
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.1.1/ckeditor_4.1.1_standard.zip"

libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/archive/version/2.1.zip"
