<?php
/**
 * @file ./gocms.inc
 *
 * Helper functions
 */

/**
 * The available modules.
 */
function gocms_available_modules() {
  $matches = drupal_system_listing("/^" . DRUPAL_PHP_FUNCTION_PATTERN . "\.module$/", '', 'name', 0);
  foreach ($matches as $module_name => $details) {
    $list = '_test' !== substr($module_name, -5);
    $list = $list && '_example' !== substr($module_name, -8);
    if ($list) {
      $name = ucwords(str_replace('_', ' ', $module_name));
      $options[$module_name] = $name;
    }
    ksort($options);
  }
  return $options;
}

/**
 * Function for debugging, copied from devel.module, remove access checking.
 */
function _kpr($input, $return = FALSE, $name = NULL, $function = 'print_r') {
  module_load_include('module', 'devel');
  // We do not want to krumo() strings and integers and such
  if (merits_krumo($input)) {
    return $return ? (isset($name) ? $name .' => ' : '') . krumo_ob($input) : krumo($input);
  }
  return dprint_r($input, $return, $name, $function);
}

/**
 * Available preconfigured-features.
 *
 * @return array
 */
function gocms_available_preconfigured_features() {
  $fs['input_format'] = 'Create two basic input formats: Filtered HTML, Full HTML';
  $fs['content_types'] = 'Create two basic content type: Page, Article';
  $fs['admin'] = 'Admin (Seven Theme)';
  # $fs['admin'] = 'Admin (Seven Theme, Contextual, Toolbar, Module Filter)';
  return $fs;
}
/**
 * API function to enable a preconfigured feature.
 * @param  string $f
 */
function gocms_enable_preconfigured_feature($f) {
  switch ($f) {
    case 'input_format':
    case 'content_types':
    case 'admin':
      $callback = "gocms_enable_preconfigured_feature__{$f}";
      break;
  }

  if (!empty($callback) && function_exists($callback)) {
    $callback();
  }
}

function gocms_enable_preconfigured_feature__input_format() {
  // Add text formats.
  $filtered_html_format = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'weight' => 0,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // HTML filter.
      'filter_html' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $filtered_html_format = (object) $filtered_html_format;
  filter_format_save($filtered_html_format);

  $full_html_format = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'weight' => 1,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $full_html_format = (object) $full_html_format;
  filter_format_save($full_html_format);
}

function gocms_enable_preconfigured_feature__content_types() {
  gocms_enable_preconfigured_feature__input_format();

  // Insert default pre-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Basic page'),
      'base' => 'node_content',
      'description' => st("Use <em>basic pages</em> for your static content, such as an 'About us' page."),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
    array(
      'type' => 'article',
      'name' => st('Article'),
      'base' => 'node_content',
      'description' => st('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
  );

  foreach ($types as $type) {
    $type = node_type_set_defaults($type);
    node_type_save($type);
    node_add_body_field($type);
  }
}

function gocms_enable_preconfigured_feature__admin() {
  variable_set('admin_theme', 'seven');
  variable_set('node_admin_theme', '1');
}
